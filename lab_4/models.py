from django.db import models
import datetime
import pytz

now = datetime.datetime.now()


class Message(models.Model):
    name = models.CharField(max_length=27)
    email = models.EmailField()
    message = models.TextField()
    created_date = models.DateTimeField(default = now.replace(tzinfo = pytz.UTC))

    def __str__(self):
        return self.message
