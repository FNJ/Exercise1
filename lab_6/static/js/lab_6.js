localStorage.theme = '['+
  '{"id":0,"text":"Red","bcgColor":"#F44336","fontColor":"#212121"},'+
  '{"id":1,"text":"Pink","bcgColor":"#E91E63","fontColor":"#212121"},'+
  '{"id":2,"text":"Purple","bcgColor":"#9C27B0","fontColor":"#212121"},'+
  '{"id":3,"text":"Indigo","bcgColor":"#3F51B5","fontColor":"#212121"},'+
  '{"id":4,"text":"Blue","bcgColor":"#2196F3","fontColor":"#212121"},'+
  '{"id":5,"text":"Teal","bcgColor":"#009688","fontColor":"#212121"},'+
  '{"id":6,"text":"Lime","bcgColor":"#CDDC39","fontColor":"#212121"},'+
  '{"id":7,"text":"Yellow","bcgColor":"#FFEB3B","fontColor":"#212121"},'+
  '{"id":8,"text":"Amber","bcgColor":"#FFC107","fontColor":"#212121"},'+
  '{"id":9,"text":"Orange","bcgColor":"#FF5722","fontColor":"#212121"},'+
  '{"id":10,"text":"Brown","bcgColor":"#795548","fontColor":"#212121"}]';

// Calculator
var print = document.getElementById('print');
var erase = false;

var go = function(x) {
  if (x === 'ac') {
    /* implemetnasi clear all */
    print.value = '';
  }
  else if (x === 'eval') {
    print.value = Math.round(evil(print.value) * 10000) / 10000;
    erase = true;
  }
  else if (x === 'sin') {
    print.value = Math.round(evil(Math.sin(print.value)) * 10000) / 10000;
    erase = true;
  }
  else if (x === 'tan') {
    print.value = Math.round(evil(Math.tan(print.value)) * 10000) / 10000;
    erase = true;
  }
  else if (x === 'log') {
    print.value = Math.round(evil(Math.log(print.value)) * 10000) / 10000;
    erase = true;
  }
  else {
    print.value += x;
  }
};
function evil(fn) {
    return new Function('return ' + fn)();
}
  // END

$(".chat-text").on('KeyboardEvent', function(){
})

$(document).ready(function(){
  $('.my-select').select2({
    "data" : JSON.parse(localStorage.theme)
  });
  if (localStorage.getItem("selectedTheme") == null){
    localStorage.selectedTheme = JSON.stringify(JSON.parse(localStorage.theme)[3]);
    theme = JSON.parse(localStorage.selectedTheme);
    gantiWarna(theme.bcgColor, theme.fontColor);
  }
  else{
    theme = JSON.parse(localStorage.selectedTheme);
    gantiWarna(theme.bcgColor, theme.fontColor);
  }
});

function gantiWarna(bg, font){
  $('body').css('background', bg);
  $('body').css('color' , font);
}

$('.apply-button').on('click', function(){
  var datanya = $('.my-select').val();
  var theme = JSON.parse(localStorage.theme)
  for (i = 0;i<theme.length;i++){
    if (datanya == theme[i].id){
      var selected = theme[i];
      gantiWarna(selected.bcgColor, selected.fontColor);
      localStorage.selectedTheme = JSON.stringify(theme[i]);
      break;
    }
  }
})
$('textarea').on('keydown', function(e){
  if (e.keyCode == 13){
    messageappend($('textarea').val());
  }
})
$('textarea').on('keyup', function(e){
  if (e.keyCode == 13){
    $("textarea").val("");
  }
})
var sign = 0;
function messageappend(e){
  if(sign == 0){
    $('.msg-insert').append('<div class = "msg-send">'+e+'</div>');
    sign = 1;
    return;
  }
  else{
    $('.msg-insert').append('<div class = "msg-receive">'+e+'</div>');
    sign = 0;
    return;
  }
}