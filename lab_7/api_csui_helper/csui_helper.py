import requests
import os
import environ

root = environ.Path(__file__) - 3 # three folder back (/a/b/c/ - 3 = /)
env = environ.Env(DEBUG=(bool, False),)
environ.Env.read_env('.env')
API_MAHASISWA_LIST_URL = "https://api.cs.ui.ac.id/siakngcs/mahasiswa-list/"

class CSUIhelper:
    def __init__(self, usernya, passwordnya):
        if (usernya == ''):
            self.username = env("SSO_USERNAME")
        else :
            self.username = usernya
        if (passwordnya == ''):
            self.password = env("SSO_PASSWORD")
        else :
            self.password = passwordnya    
        self.client_id = 'X3zNkFmepkdA47ASNMDZRX3Z9gqSU1Lwywu5WepG'
        self.access_token = self.get_access_token()

    def get_access_token(self):
        url = "https://akun.cs.ui.ac.id/oauth/token/"

        payload = "username=" + self.username + "&password=" + self.password + "&grant_type=password"
        headers = {
                'authorization': "Basic WDN6TmtGbWVwa2RBNDdBU05NRFpSWDNaOWdxU1UxTHd5d3U1V2VwRzpCRVFXQW43RDl6a2k3NEZ0bkNpWVhIRk50Ymg3eXlNWmFuNnlvMU1uaUdSVWNGWnhkQnBobUU5TUxuVHZiTTEzM1dsUnBwTHJoTXBkYktqTjBxcU9OaHlTNGl2Z0doczB0OVhlQ3M0Ym1JeUJLMldwbnZYTXE4VU5yTEFEMDNZeA==",
                'cache-control': "no-cache",
                'content-type': "application/x-www-form-urlencoded"
        }

        response = requests.request("POST", url, data=payload, headers=headers)
        try:
            return response.json()["access_token"]
        except Exception:
            return 0

    def get_client_id(self):
        return self.client_id

    def get_auth_param_dict(self):
        dict = {}
        acces_token = self.get_access_token()
        client_id = self.get_client_id()
        dict['access_token'] = acces_token
        dict['client_id'] = client_id

        return dict

    def get_mahasiswa_list(self):
        if (self.access_token == 0):
            return []
        else :
            response = requests.get(API_MAHASISWA_LIST_URL,
                                    params={"access_token": self.access_token, "client_id": self.client_id})
            mahasiswa_list = response.json()["results"]
            return mahasiswa_list