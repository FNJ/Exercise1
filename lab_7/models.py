from django.db import models

class Friend(models.Model):
    friend_name = models.CharField(max_length=400)
    # alamat = models.TextField(default='')
    npm = models.CharField(max_length=250)
    added_at = models.DateField(auto_now_add=True)

    def as_dict(self):
        return {
            "pk" : self.pk,
            "friend_name": self.friend_name,
            "npm": self.npm,
            # 'alamat':self.alamat,
            }
