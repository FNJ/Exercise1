# from django.test import TestCase
# from django.test import Client
# from django.urls import resolve
# from django.http import HttpRequest
# from .views import index, friend_list, friend_list_json, add_friend, delete_friend, validate_npm
# from .models import Friend
# from .api_csui_helper.csui_helper import CSUIhelper

# # Create your tests here.
# class Lab7UnitTest(TestCase):

#     def test_lab_7_url_is_exist(self):
#         response = Client().get('/lab-7/')
#         self.assertEqual(response.status_code, 200)

#     def test_lab7_using_index_func(self):
#         found = resolve('/lab-7/')
#         self.assertEqual(found.func, index)

#     def test_lab7_url_friend_list(self) :
#         response = Client().get('/lab-7/get-friend-list/')
#         self.assertEqual(response.status_code, 200)
    
#     def test_lab7_can_collect_all_data_from_API(self):
#         csui_helper = CSUIhelper("","")
#         mahasiswa_list = csui_helper.get_mahasiswa_list()
#         self.assertNotEqual(len(mahasiswa_list), 0)

#     def test_lab7_can_not_collect_all_data_from_API(self):
#         csui_helper = CSUIhelper("fran","hehe")
#         mahasiswa_list = csui_helper.get_mahasiswa_list()
#         self.assertEqual(len(mahasiswa_list), 0)

#     def test_lab7_model_can_output_dict_of_object(self):
#         temen = Friend(friend_name= "fran", npm="12345")
#         dictio = temen.as_dict()
#         self.assertEqual("fran", dictio["friend_name"])

#     def test_lab7_friend_list_json_worked(self):
#         temen = Friend(friend_name= "fran", npm="12345")
#         temen.save()
#         request = HttpRequest()
#         response = friend_list_json(request)
#         self.assertJSONEqual(str(response.content, encoding='utf8'), {'results':[{'friend_name':'fran', 'npm':'12345', 'pk':1}]})

#     def test_lab7_can_add_friend(self):
#         request = HttpRequest()
#         request.method = 'POST'
#         request.POST = {'name' : "Fran Na Jaya" , 'npm' : '1606837234'}
#         response = add_friend(request)

#         count_friend_added = Friend.objects.all().count()
#         self.assertEqual(count_friend_added, 1)

#     def test_lab7_can_delete_friend(self):
#         temen = Friend(friend_name= "fran", npm="12345")
#         temen.save()
#         count_friend_added = Friend.objects.all().count()
#         self.assertEqual(count_friend_added, 1)
#         request = HttpRequest()
#         response = delete_friend(request, 1)
#         count_friend_delete = Friend.objects.all().count()
#         self.assertEqual(count_friend_delete, 0)

#     def test_lab7_can_check_npm(self):
#         request = HttpRequest()
#         request.method = 'POST'
#         request.POST = {'name' : "Fran Na Jaya" , 'npm' : '1606837234'}
#         response = add_friend(request)
#         request.POST = {'npm' : '16'}
#         responseValid = validate_npm(request)
#         self.assertJSONEqual(str(responseValid.content, encoding='utf8'), {'is_taken':False})

#     def test_lab7_can_check_added_friend_already_add(self):
#         request = HttpRequest()
#         request.method = 'POST'
#         request.POST = {'name' : "Fran Na Jaya" , 'npm' : '1606837234'}
#         response = add_friend(request)
#         request.POST = {'npm' : '1606837234'}
#         responseValid = validate_npm(request)
#         self.assertJSONEqual(str(responseValid.content, encoding='utf8'), {'is_taken':True})

#     def test_lab7_can_return_client_id(self):
#         csui_helper = CSUIhelper("","")
#         client_id = csui_helper.get_client_id()
#         self.assertEqual(client_id,'X3zNkFmepkdA47ASNMDZRX3Z9gqSU1Lwywu5WepG')

#     def test_lab7_can_return_param_dict(self):
#         csui_helper = CSUIhelper("fran","hehe")
#         param_dict = csui_helper.get_auth_param_dict()
#         self.assertDictEqual(param_dict, {'access_token':0, 'client_id':'X3zNkFmepkdA47ASNMDZRX3Z9gqSU1Lwywu5WepG'})

    