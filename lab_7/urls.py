from django.conf.urls import url
from .views import index, add_friend, validate_npm, friend_list, delete_friend, friend_list_json

urlpatterns = [
    url(r'^$', index, name='index'),
    url(r'^add-friend/$', add_friend, name='add-friend'),
    url(r'^validate-npm/$', validate_npm, name='validate-npm'),
    url(r'^get-friend-list/delete-friend/(?P<friend_pk>\d+)/$', delete_friend, name='delete-friend'),
    url(r'^get-friend-list/$', friend_list, name='get-friend-list'),
    url(r'^get-friend-list/ambil$', friend_list_json, name='get-friend-list-ambil'),
    # url(r'^get-friend-list/peta-friend/?alamat =', peta, name='peta-friend'),

]