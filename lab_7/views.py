from django.shortcuts import render
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger
from django.http import HttpResponse, HttpResponseRedirect, JsonResponse
from django.views.decorators.csrf import csrf_exempt
from django.core import serializers
from .models import Friend
from .api_csui_helper.csui_helper import CSUIhelper
import os
import json

response = {"author" : "Fran Na jaya"}
csui_helper = CSUIhelper("","")

def index(request):
    # Page halaman menampilkan list mahasiswa yang ada
    # TODO berikan akses token dari backend dengan menggunakaan helper yang ada

    page = request.GET.get('page', 3)
    mahasiswa_cs = csui_helper.get_mahasiswa_list()
    paginator = Paginator(mahasiswa_cs, 10)
    friend_list = Friend.objects.all()
    mahasiswa_list = paginator.page(page)

    response = {"mahasiswa_list": mahasiswa_list, "friend_list": friend_list}
    html = 'lab_7/lab_7.html'

    return render(request, html, response)

#  url, npm, nama, alamat_mhs, kd_pos_mhs, kota_lahir, tgl_lahir, program

def friend_list(request):
    
    
    html = 'lab_7/daftar_teman.html'
    return render(request, html, response)

def friend_list_json(request): # update
    friends = [obj.as_dict() for obj in Friend.objects.all()]
    return JsonResponse({"results": friends}, content_type='application/json')

@csrf_exempt
def add_friend(request):
    if request.method == 'POST':
        name = request.POST['name']
        npm = request.POST['npm']
        data =Friend.objects.filter(npm = npm)
        if (len(data)==0):
            friend = Friend(friend_name=name, npm=npm)
            friend.save()

            return JsonResponse(friend.as_dict())

def delete_friend(request, friend_pk):
    Friend.objects.filter(pk=friend_pk).delete()
    return HttpResponseRedirect('/lab-7/get-friend-list/')

@csrf_exempt
def validate_npm(request):
    npm = request.POST.get('npm', None)
    list = Friend.objects.filter(npm=npm)
    data = {}
    if len(list) > 0:
        data['is_taken'] = True
    else :
        data['is_taken'] = False
    return JsonResponse(data)

# def peta(request):
#     alamat = request.GET['alamat']
#     response['alamat'] = alamat
#     html = 'lab_7/mymap.html'
#     return render(request, html, response)