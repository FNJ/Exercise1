(function(d, s, id) {
  var js, fjs = d.getElementsByTagName(s)[0];
  if (d.getElementById(id)) return;
  js = d.createElement(s); js.id = id;
  js.src = 'https://connect.facebook.net/en_GB/sdk.js#xfbml=1&version=v2.11';
  fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));

window.fbAsyncInit = function() {
  FB.init({
    appId      : '2083205731903385',
    cookie     : true,
    xfbml      : true,
    version    : 'v2.11'
  });

  FB.getLoginStatus(function(response) {
    statusChangeCallback(response);
  });


};

$( window ).on("load", function () {
  checkLoginState();
  console.log("selesai");
});

// This is called with the results from from FB.getLoginStatus().
function statusChangeCallback(response) {
  console.log('statusChangeCallback');
  console.log(response);
  // The response object is returned with a status field that lets the
  // app know the current login status of the person.
  // Full docs on the response object can be found in the documentation
  // for FB.getLoginStatus().
  
  if (response.status === 'connected') {
    // Logged into your app and Facebook.
    render(true);
  } else {
    // The person is not logged into your app or we are unable to tell.
    render(false);
  }
}

// This function is called when someone finishes with the Login
function checkLoginState() {
  FB.getLoginStatus(function(response) {
    statusChangeCallback(response);
  });
}

function facebookLogin(){
  FB.login(function(response){
    console.log(response);
  }, {scope:'profile,user_posts,publish_actions'})
}
const render = loginFlag => {
  console.log("sampe");
  if (loginFlag) {
    console.log("bener");
    // Jika yang akan dirender adalah tampilan sudah login
    // Memanggil method getUserData (lihat ke bawah) yang Anda implementasi dengan fungsi callback
    // yang menerima object user sebagai parameter.
    // Object user ini merupakan object hasil response dari pemanggilan API Facebook.
    getUserData(user => {
      // Render tampilan profil, form input post, tombol post status, dan tombol logout
      $('#fb-root').append(
        '<div class="profile">' +
          '<img class="cover" src="' + user.cover.source + '" alt="cover" />' +
          '<img class="picture" src="' + user.picture.data.url + '" alt="profpic" />' +
          '<div class="data">' +
            '<h1>' + user.name + '</h1>' +
            '<h2>' + user.about + '</h2>' +
            '<h3>' + user.email + ' - ' + user.gender + '</h3>' +
          '</div>' +
        '</div>' +
        '<input id="postInput" type="text" class="post" placeholder="Ketik Status Anda" />' +
        '<button class="postStatus" onclick="postStatus()">Post ke Facebook</button>'
      );

      // Setelah merender tampilan di atas, dapatkan data home feed dari akun yang login
      // dengan memanggil method getUserFeed yang kalian implementasi sendiri.
      // Method itu harus menerima parameter berupa fungsi callback, dimana fungsi callback
      // ini akan menerima parameter object feed yang merupakan response dari pemanggilan API Facebook
      getUserFeed(feed => {

        feed.data.map(value =>
        {

          // Render feed, kustomisasi sesuai kebutuhan.
          if (value.message && value.story)
          {
            $('fb-root').append(
              '<div class="feed">' +
                '<h1>' + value.message + '</h1>' +
                '<h2>' + value.story + '</h2>' +
              '</div>'
            );
          }
          else if (value.message)
          {
            $('#fb-root').append(
              '<div class="feed">' +
                '<h1>' + value.message + '</h1>' +
              '</div>'
            );
          }
          else if (value.story) 
          {
            $('#fb-root').append(
              '<div class="feed">' +
                '<h2>' + value.story + '</h2>' +
              '</div>'
            );
          }

        });

      });
    });
  }
  else
  {
    // Tampilan ketika belum login
    console.log("salah");
    var hehe = 'silahkan Login terlebih dahulu';
    var buttonfb  = '<button id = "login" onClick = "facebookLogin()">Log in with Facebook</button>';
    $('#status').append(hehe);
    $('#fb-btn-place').append(buttonfb);
  }
};

// TODO: Lengkapi Method Ini
// Method ini memodifikasi method getUserData di atas yang menerima fungsi callback bernama fun
// lalu merequest data user dari akun yang sedang login dengan semua fields yang dibutuhkan di 
// method render, dan memanggil fungsi callback tersebut setelah selesai melakukan request dan 
// meneruskan response yang didapat ke fungsi callback tersebut
// Apakah yang dimaksud dengan fungsi callback?
const getUserData = (fun) => {
  FB.api('/me?fields=name,about, email,gender, picture, cover', 'GET', function (response){
    console.log(response);
    fun(response);
  });
};

  const getUserFeed = (fun) => {
    // TODO: Implement Method Ini
    // Pastikan method ini menerima parameter berupa fungsi callback, lalu merequest data Home Feed dari akun
    // yang sedang login dengan semua fields yang dibutuhkan di method render, dan memanggil fungsi callback
    // tersebut setelah selesai melakukan request dan meneruskan response yang didapat ke fungsi callback
    // tersebut
  };

  function facebookLogout(){
    FB.getLoginStatus(function(response) {
       if (response.status === 'connected') {
         FB.logout();
       }
    });
  }
  
  function postFeed(){
    var feed = $('#post').val();
    FB.api('/me/feed', 'POST', {message:feed});
  }
  




////////////////////////////////////////////////////////// UN USED BLOCK ////////////////////////////////////////////////
// // Here we run a very simple test of the Graph API after login is
// // successful.  See statusChangeCallback() for when this call is made.
// function testAPI() {
//   console.log('Welcome!  Fetching your information.... ');
//   // melakukan request ke path '/me'
//   FB.api('/me', function(response) {
//     console.log(response);
//     console.log('Successful login for: ' + response.name);
//     document.getElementById('status').innerHTML =
//       'Thanks for logging in, ' + response.name + '!';
//   });
// }
