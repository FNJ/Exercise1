(function(d, s, id) {
  var js, fjs = d.getElementsByTagName(s)[0];
  if (d.getElementById(id)) return;
  js = d.createElement(s); js.id = id;
  js.src = 'https://connect.facebook.net/en_GB/sdk.js#xfbml=1&version=v2.11';
  fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));

window.fbAsyncInit = function() {
  FB.init({
    appId      : '2083205731903385',
    cookie     : true,
    xfbml      : true,
    version    : 'v2.11'
  });

  FB.getLoginStatus(function(response) {
    statusChangeCallback(response);
  });
};

//ini untuk melakukan call back and rendering setelahnya
function statusChangeCallback(response) {
  console.log('statusChangeCallback');
  console.log(response);
  // The response object is returned with a status field that lets the
  // app know the current login status of the person.
  // Full docs on the response object can be found in the documentation
  // for FB.getLoginStatus().
  
  if (response.status === 'connected') {
    // Logged into your app and Facebook.
    return render(true);
  } else {
    // The person is not logged into your app or we are unable to tell.
    return render(false);
  }
}

// This function is called when someone finishes with the Login
function checkLoginState() {
  FB.getLoginStatus(function(response) {
    statusChangeCallback(response);
  });
}

const render = loginFlag => {
  console.log("sampe");
  if (loginFlag) {
    console.log("bener");
    $("#item").empty();
    // Jika yang akan dirender adalah tampilan sudah login
    // Memanggil method getUserData (lihat ke bawah) yang Anda implementasi dengan fungsi callback
    // yang menerima object user sebagai parameter.
    // Object user ini merupakan object hasil response dari pemanggilan API Facebook.
    getUserData(user => {
      // Render tampilan profil, form input post, tombol post status, dan tombol logout
      $('#item').append(
        '<div class="profile">' +
          '<img class="cover" src="' + user.cover.source + '" alt="cover" />' +
            '<div class="picture">' +
              '<img src="' + user.picture.data.url + '" alt="profpic" />' +'<span>'+ user.name +
            '</span></div>' +
        '</div>' +
        '<div class="data">' +
          '<h1>'+user.email + ' - ' + user.gender + '</h1>' +
          '<h2><b>ABOUT</b><br>'+user.about + '</h2>'+
        '</div>' +
        '<input id = "postInput" type = "text" class = "post" placeholder = "Ketik Status Anda" />' +
        '<button id = "postStatus" onClick="postStatus()">Post ke Facebook</button>'
      );

      // Setelah merender tampilan di atas, dapatkan data home feed dari akun yang login
      // dengan memanggil method getUserFeed yang kalian implementasi sendiri.
      // Method itu harus menerima parameter berupa fungsi callback, dimana fungsi callback
      // ini akan menerima parameter object feed yang merupakan response dari pemanggilan API Facebook
      getUserFeed(things => {
        console.log(things.feed.data);
        things.feed.data.map(value =>
        {
          console.log(value);
          // Render feed, kustomisasi sesuai kebutuhan.
          if (value.message && value.story)
          {
            $('item').append(
              '<div class="feed">' +
                '<div id="feedpp"'+
                  '<img src="' + things.picture.data.url + '" alt="profpic" />' +'<span>'+ things.name +
                  '</span></div>' +
                  '<div class="time">'+value.created_time+'</div>'+
                '<h1>' + value.message + '</h1>' +
                '<h2>' + value.story + '</h2>' +
                '<button class = "deletePost" onClick="deleteFbpost(\''+value.id+'\')"> delete post&times</button>'+ 
              '</div>'
            );
          }
          else if (value.message)
          {
            $('#item').append(
              '<div class="feed">' +
                '<img src="' + things.picture.data.url + '" alt="profpic" />' +'<span>'+ things.name +
                '</span>' +
                '<div class="time">'+value.created_time+'</div>'+
                '<h1>' + value.message + '</h1>' +
                '<button class = "deletePost" onClick="deleteFbpost(\''+value.id+'\')"> delete post&times</button>'+ 
              '</div>'
            );
          }
          else if (value.story) 
          {
            $('#item').append(
              '<div class="feed">' +
                '<img src="' + things.picture.data.url + '" alt="profpic" />' +'<span>'+ things.name +
                '</span>' +
                '<div class="time">'+value.created_time+'</div>'+
                '<h2>' + value.story + '</h2>' +
                '<button class = "deletePost" onClick="deleteFbpost(\''+value.id+'\')"> delete post&times</button>'+ 
              '</div>'
            );
          }

        });

      });
    });
  }
  else
  {
    // Tampilan ketika belum login
    console.log("salah");
    $("#item").empty();
  }
};

// TODO: Lengkapi Method Ini
// Method ini memodifikasi method getUserData di atas yang menerima fungsi callback bernama fun
// lalu merequest data user dari akun yang sedang login dengan semua fields yang dibutuhkan di 
// method render, dan memanggil fungsi callback tersebut setelah selesai melakukan request dan 
// meneruskan response yang didapat ke fungsi callback tersebut
// Apakah yang dimaksud dengan fungsi callback?
const getUserData = (fun) => {
  FB.api('/me?fields=name,about, email,gender, picture, cover', 'GET',
    function (response){
      console.log(response);
      fun(response);
    }
  );
};

const getUserFeed = (fun) => {
  FB.api(
    "/me?fields=name, picture, feed", 'GET', 
    function (response) {
      console.log(response);
      if (response && !response.error) {
        fun(response);
      }
    }
  );
};

function postStatus(){
  var feed = $('#postInput').val();
  console.log(feed);
    /* make the API call */
  FB.api(
    "/me/feed",
    "POST",
    {
        "message": feed
    },
    function (response) {
      if (response && !response.error) {
        checkLoginState();
      }
    }
  );
}

function deleteFbpost(hehe){
  /* make the API call */
  console.log(hehe);
  FB.api(
  "/"+hehe+"",
  "DELETE",
  function (response) {
    console.log(response);
    if (response && !response.error) {
      console.log("delete"+hehe);
      checkLoginState();
    }
  }
  );
}
